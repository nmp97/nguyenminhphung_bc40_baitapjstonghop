// EX1
const tbody = document.querySelector('tbody');

for (let i = 0; i < 10; i++) {
  const tr = document.createElement('tr');
  for (let j = 1; j <= 10; j++) {
    const td = document.createElement('td');
    td.appendChild(document.createTextNode(i * 10 + j));
    tr.appendChild(td);
  }
  tbody.appendChild(tr);
}

document.querySelector('table').setAttribute('class', 'table table-striped');
document.querySelectorAll('tr').forEach((item) => {
  item.style.textAlign = 'center';
});

// EX2
const checkPrime = (n) => {
  if (n < 2) return false;
  for (let i = 2; i < n - 1; i++) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
};

const ex2Arr = [];
document.getElementById('ex2Btn').onclick = () => {
  const ex2Input = document.getElementById('ex2InputValue').value * 1;
  ex2Arr.push(ex2Input);
  document.getElementById('exArrInput').innerHTML = ex2Arr;
  document.getElementById('ex2InputValue').value = '';
};

var primeList = [];
document.getElementById('ex2Find').onclick = () => {
  const len = ex2Arr.length;

  for (let i = 0; i < len; i++) {
    if (checkPrime(ex2Arr[i])) {
      primeList.push(ex2Arr[i]);
    }
  }
  document.getElementById('result').innerHTML = primeList;
  primeList = [];
};

// EX3
document.getElementById('ex3Btn').onclick = () => {
  let num = document.getElementById('ex3Input').value * 1;
  let sum = 0;

  for (let i = 2; i <= num; i++) {
    sum += i;
  }

  document.getElementById('ex3result').innerHTML = `${sum + 2 * num}`;
};

// EX4
var ex4Arr = [];
document.getElementById('ex4Btn').onclick = () => {
  let num = document.getElementById('ex4Input').value * 1;

  for (let i = num; i > 0; i--) {
    if (num % i == 0) {
      ex4Arr.push(i);
    }
  }
  document.getElementById('ex4result').innerHTML = ex4Arr;
  ex4Arr = [];
};

// EX5
document.getElementById('ex5Btn').onclick = () => {
  let num = document.getElementById('ex5Input').value;

  let arrNum = num.split('');
  let reverseNum = arrNum.reverse();
  document.getElementById('ex5result').innerHTML = `${reverseNum.join('')}`;
};

// EX6
let sum = 0;
let n = 0;

while (sum + n <= 100) {
  n++;
  sum += n;
}
document.getElementById('ex6Result').innerHTML = `X=${n}`;

// EX7
document.getElementById('ex7Btn').onclick = () => {
  let num = document.getElementById('ex7Input').value * 1;
  let contentHTML = '';
  for (let i = 0; i <= 10; i++) {
    multi = num * i;
    let content = `${num} x ${i} = ${multi} </br>`;
    contentHTML += content;
  }
  document.getElementById('ex7result').innerHTML = contentHTML;
};

// EX8
var cards = ['4K', 'KH', '5C', 'KA', 'QH', 'KD', '2H', '10S', 'AS', '7H', '9K', '10D'];
var players = [[], [], [], []];

document.getElementById('ex8Btn').onclick = () => {
  while (cards.length > 0) {
    let card1 = Math.floor(Math.random() * cards.length);
    players[0].push(cards[card1]);
    cards.splice(card1, 1);

    let card2 = Math.floor(Math.random() * cards.length);
    players[1].push(cards[card2]);
    cards.splice(card2, 1);

    let card3 = Math.floor(Math.random() * cards.length);
    players[2].push(cards[card3]);
    cards.splice(card3, 1);

    let card4 = Math.floor(Math.random() * cards.length);
    players[3].push(cards[card4]);
    cards.splice(card4, 1);
  }

  document.getElementById('play1').innerHTML = `${players[0]}`;
  document.getElementById('play2').innerHTML = `${players[1]}`;
  document.getElementById('play3').innerHTML = `${players[2]}`;
  document.getElementById('play4').innerHTML = `${players[3]}`;

  cards = ['4K', 'KH', '5C', 'KA', 'QH', 'KD', '2H', '10S', 'AS', '7H', '9K', '10D'];
  players[0] = [];
  players[1] = [];
  players[2] = [];
  players[3] = [];
};

// EX9
/*
  problem: find the number of the  dog and chicken.
  know: -total chicken and dog is m,
        -total foot is n.

  solution: -call x is the number of the chicken
            -call y is the number of the dog
  we have a system of the equation: x + y = m
                                    2*x + 4*y = n
  => x = (4*m - n)/2
    y = (n - 2*m)/2
*/

document.getElementById('ex9Btn').onclick = () => {
  let m = document.getElementById('total').value * 1;
  let n = document.getElementById('totalFoot').value * 1;

  let totalChicken = (4 * m - n) / 2;
  let totalDog = (n - 2 * m) / 2;

  document.getElementById('totalChicken').innerHTML = `${totalChicken} con`;
  document.getElementById('totalDog').innerHTML = `${totalDog} con`;
};

// EX10
/*
  problem: calculate the angle of deviation of the hour and minute hands
  solution: suppose the round clock, turn from left to right
          -call: angleOfHourHand is angle of hour hand
          -angleOfMinuteHand is angle of minute hand
          -angleOfHourHandExtraMove is extra displacement angle of hour hand
          -minute hand turn a revolution 60 minutes, because the round => 360deg
  => 1 minute: 6deg; 1h = 30deg
          -clock face include 12 o'clock => 1h = 5 bars = 5 minutes = 30deg
          -if the minute hand moves one distance, the hour hand also moves
  => (1/60)=(1/30) or (6/360)=(1/30)
  => angleOfHourHandExtraMove = (angleOfMinuteHand * 6 * 30)/360
  => deviation angle = |angleOfHourHand * 5 * 6 - angleOfMinuteHand * 6| + angleOfHourHandExtraMove
*/

document.getElementById('ex10Btn').onclick = () => {
  let angleOfHourHand = document.getElementById('hour').value * 1;
  let angleOfMinuteHand = document.getElementById('minute').value * 1;

  let deviationAngle =
    Math.abs(angleOfHourHand * 5 * 6 - angleOfMinuteHand * 6) + (angleOfMinuteHand * 6 * 30) / 360;

  if (deviationAngle >= 360) {
    deviationAngle = Math.abs(deviationAngle - 360);
  } else {
    deviationAngle;
  }

  document.getElementById('ex10Result').innerHTML = `${deviationAngle}deg`;
};
